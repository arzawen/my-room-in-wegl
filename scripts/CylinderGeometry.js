import * as THREE from 'three'
class CylinderGeometry
{
    /**
     * Constructor
     */
    constructor(radiusTop, radiusBottom, height, radialSegments, color )
    {
        this.radiusTop = radiusTop
        this.radiusBottom = radiusBottom
        this.height = height
        this.radialSegments = radialSegments
        this.color = color
    }

    /**
     * Methods
     */
    draw()
    {
        const cylinder = new THREE.Mesh(
            new THREE.CylinderGeometry(this.radiusTop, this.radiusBottom, this.height,this.radialSegments),
            new THREE.MeshStandardMaterial({ color : `#${this.color}`,metalness: 0.3, roughness: 0.8 })
        )
        return cylinder
    }
}

export default CylinderGeometry